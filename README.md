## GENERATE POSTMAN API KEY

  Go to POSTMAN > Account Settings > API Keys


## GET ALL POSTMAN COLLECTIONS

  curl https://api.getpostman.com/collections\?apikey\=$POSTMAN_API_KEY > collections.json

## RUN TESTS

  ./runTests.sh UnsuccesfulLogin,RegisterLoginAndChangePassword

## EXTRA: Run newman from Docker

  $ docker run -t postman/newman run "https://api.getpostman.com/collections/0d0350a9a89d39fb6361"

## To get URL of your Collection:

  - Select the more actions icon More actions icon next to the collection name.
  - Select Share collection.
  - Select Get public link.

## More info: (Travis CI, Jenkins, ...)

  https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman/


## TROUBLESHOOTING:

newman: command not found

  $ nvm use node

Error: Cannot find module 'newman'

  $ npm init --yes #creates default package.json
  $ npm install newman



