#!/usr/bin/env bash

DEFAULT_COLLECTION_FILTER=AccessForbidden,RegisterLoginAndAccessWithToken,RegisterLoginAndChangePassword,UnsuccesfulLogin
COLLECTION_FILTER="${1:-$DEFAULT_COLLECTION_FILTER}"

for i in `node app.js ${COLLECTION_FILTER} | awk '{print $1}'`;
    do newman run "https://api.getpostman.com/collections/$i?apikey=${POSTMAN_API_KEY}";
done;
